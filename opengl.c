#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

char* eatfile(char* filename, GLint* len)
{
	char* result = NULL;
	//Load the file
	FILE* filestream = fopen(filename,"r");

	if (filestream)
	{
		int filesize;

		//Get the file size
		fseek(filestream, 0, SEEK_END);
		filesize = ftell(filestream);
		fseek(filestream, 0, SEEK_SET);

		//Allocate room for file
		*len = filesize;
		result = calloc(1, filesize+1);
		if (result)
		{
			//Eat (Load) the file
			if(fread(result, sizeof(char), filesize, filestream) == 0)
      {
        free(result);
        result = NULL;
      }
		}
    fclose(filestream);
	}

	return result;
}

bool shaders_load(char* filename, GLenum type, GLuint* shader, const char** src)
{
	GLint compile_status;
	GLint shader_len = 0;

	*shader = glCreateShader(type);
	*src = eatfile(filename, &shader_len);
	glShaderSource(*shader, 1, src, &shader_len);
	glCompileShader(*shader);
	glGetShaderiv(*shader,  GL_COMPILE_STATUS, &compile_status);

	if(!compile_status)
	{
		char buf[300];
		glGetShaderInfoLog(*shader, 300, NULL, buf);
		puts(buf);
	}
	return (compile_status != 0);
}

GLuint shaders_init(char* vshader, char* fragshader)
{
	GLuint program = 0;
	const char* shader_vertex_src, *shader_fragment_src;
	GLuint shader_vertex, shader_fragment;

	shaders_load(vshader, GL_VERTEX_SHADER, &shader_vertex, &shader_vertex_src);
	shaders_load(fragshader, GL_FRAGMENT_SHADER, &shader_fragment, &shader_fragment_src);

	program = glCreateProgram();
	glAttachShader(program, shader_vertex);
	glAttachShader(program, shader_fragment);
	glLinkProgram(program);

	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) 
	{
		char log[512];
		glGetProgramInfoLog(program, 512, NULL, log);
		puts(log);
	}

	glDeleteShader(shader_vertex);
	glDeleteShader(shader_fragment);

	free((void*) shader_vertex_src);
	free((void*) shader_fragment_src);

	return program;
}

GLuint object_load(GLfloat* vertex_data)
{
	GLuint varray;
	glGenVertexArrays(1, &varray);

	glBindVertexArray(varray);

	GLuint vbuffer;
	glGenBuffers(1, &vbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
	glBufferData(GL_ARRAY_BUFFER, 6*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);

	return varray;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(500, 500, "Hello", NULL, NULL);
	if(!window)
	{
		printf("Uh oh.\n");
		exit(1);
	}

	glfwMakeContextCurrent(window);
	if(gl3wInit())
	{
		printf("I'm Glad that's broken.\n");
		exit(1);
	}

	GLfloat vertex_data1[6] = {
		-0.5, -0.5, 
		 0.5, -0.5, 
		 0.5,  0.5};

	GLfloat vertex_data2[6] = {
		-0.5, -0.4, 
		-0.5,  0.5, 
		 0.4,  0.5
	};

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glViewport(0,0,500,500);

	GLint varray1 = object_load(vertex_data1);
	GLuint program1 = shaders_init(SRC_PATH "/shaders/vertex.glsl", SRC_PATH "/shaders/fragment.glsl");

	GLint varray2 = object_load(vertex_data2);
	GLuint program2 = shaders_init(SRC_PATH "/shaders/vertex.glsl", SRC_PATH "/shaders/fragment2.glsl");

	while(!glfwWindowShouldClose(window))
	{
		glfwSwapBuffers(window);
		glfwPollEvents();

		glBindVertexArray(varray1);
		glUseProgram(program1);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		glBindVertexArray(varray2);
		glUseProgram(program2);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}

	glfwTerminate();

	return 0;
} 
